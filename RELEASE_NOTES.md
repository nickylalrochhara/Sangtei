# Release Notes for Sangtei Bot

## Version X.Y.Z

### Features

- This is the answer for our community member request we add new languages 'Mizo, Hmar, Kuki, Lai & Mara.
- We publish Docker image for easy deployedment and reduce time for setting up Miss Sangtei.

### Enhancements

- ### Enhancements

- Improved user interface for better user experience.
- Added new command to display current server status.

### Bug Fixes

- We fix button not responding when clicking modules button on Miss Sangtei.
- Before when some user clicking ,odules button, it's alway lag and not respond, we recently fix this bug.

### Other Changes

- We host Miss Sangtei from our local machine before, but is always down when we don't keep running our machine, this is bad idea for hsoting our development, we recently moved our host to Oracle Cloud Host, now you can use Miss Sangtei without slow speed or down.
- We include donate button or modules, for our project if someone want to support or contribute and want to keep maintain our bot they can contribute using donate by clicking /donate.
...
